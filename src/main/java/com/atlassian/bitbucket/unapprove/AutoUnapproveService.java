package com.atlassian.bitbucket.unapprove;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scope.Scope;

import javax.annotation.Nonnull;

public interface AutoUnapproveService {

    /**
     * Find if approvals should be removed after new changes to a pull request in this scope
     *
     * @param scope the scope representing the project or repository
     * @return a {@link AutoUnapproveSettings} representing the configured settings
     * @since 4.1
     */
    @Nonnull
    AutoUnapproveSettings getSettings(Scope scope);

    /**
     * Find if approvals should be removed after new changes to the given pull request.
     *
     * @param pullRequest the pull request
     * @return a {@link AutoUnapproveSettings} representing the configured settings
     * @since 4.1
     */
    @Nonnull
    AutoUnapproveSettings getSettings(@Nonnull PullRequest pullRequest);

    /**
     * Find if approvals should be removed after new changes to the given pull request.
     *
     * @param pullRequest the pull request
     * @return {@code true} if approvals should be removed, {@code false} otherwise
     * @deprecated for removal in the next major release. Use {@link #getSettings(PullRequest)} instead
     */
    @Deprecated
    boolean isEnabled(@Nonnull PullRequest pullRequest);

    /**
     * Find if approvals should be removed after new changes to a pull request in this repository
     *
     * @param repository the repository
     * @return {@code true} if approvals should be removed, {@code false} otherwise
     * @deprecated for removal in the next major release. Use {@link #getSettings(Scope)} instead
     */
    @Deprecated
    boolean isEnabled(@Nonnull Repository repository);

    /**
     * Remove the settings for the given scope. Removing the settings for a repository will mean that it inherits the
     * settings from its project. Removing the settings from a project means that it inherits the global settings.
     * The global settings cannot be removed.
     *
     * @param scope the scope to remove the settings for
     * @return {@code true} if the settings were successfully removed, {@code false} otherwise
     * @since 4.1
     */
    boolean removeSettings(@Nonnull Scope scope);

    /**
     * Set unapproval to be enabled or disabled
     *
     * @param repository the repository
     * @param enabled {@code true} if approvals should be removed, {@code false} otherwise
     * @deprecated for removal in the next major release. Use {@link #setSettings(AutoUnapproveSettings)} instead
     */
    @Deprecated
    void setEnabled(@Nonnull Repository repository, boolean enabled);

    /**
     * Set auto-unapproval settings
     *
     * @param settings an object representing the settings to be set
     * @since 4.1
     */
    void setSettings(@Nonnull AutoUnapproveSettings settings);
}
