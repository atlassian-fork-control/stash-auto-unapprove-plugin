package com.atlassian.bitbucket.internal.unapprove.rest;

import com.atlassian.bitbucket.rest.util.ResourcePatterns;
import com.atlassian.bitbucket.unapprove.AutoUnapproveService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.Path;

@AnonymousAllowed
@Path(ResourcePatterns.PROJECT_URI + "/settings")
@Singleton
public class AutoUnapproveProjectSettingsResource extends AbstractAutoUnapproveSettingsResource {

    public AutoUnapproveProjectSettingsResource(AutoUnapproveService unapproveService) {
        super(unapproveService);
    }
}
