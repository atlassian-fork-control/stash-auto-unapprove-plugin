package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.DataStoreException;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionValidationService;
import com.atlassian.bitbucket.pull.*;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scope.*;
import com.atlassian.bitbucket.unapprove.AutoUnapproveGlobalSettingsChangedEvent;
import com.atlassian.bitbucket.unapprove.AutoUnapproveProjectSettingsChangedEvent;
import com.atlassian.bitbucket.unapprove.AutoUnapproveSettings;
import com.atlassian.bitbucket.unapprove.AutoUnapproveSettingsChangedEvent;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.bitbucket.validation.ArgumentValidationException;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Optional;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

/**
 * @since 2.2
 */
public class DefaultAutoUnapproveService implements InternalAutoUnapproveService {

    /**
     * The plugin key no longer matches the Maven {@code groupId} and {@code artifactId}, but it can't be changed
     * because the {@link PluginSettings} are associated with this key, as well as its Marketplace listing.
     */
    static final String PLUGIN_KEY = "com.atlassian.stash.plugin.stash-auto-unapprove-plugin";

    private static final String GLOBAL_SETTINGS_KEY = "global.auto.unapprove";
    private static final Logger log = LoggerFactory.getLogger(DefaultAutoUnapproveService.class);

    private final EventPublisher eventPublisher;
    private final I18nService i18nService;
    private final PluginSettings pluginSettings;
    private final PullRequestService pullRequestService;
    private final SecurityService securityService;
    private final TransactionTemplate transactionTemplate;
    private final PermissionValidationService validationService;

    public DefaultAutoUnapproveService(EventPublisher eventPublisher, I18nService i18nService,
                                       PluginSettingsFactory pluginSettingsFactory,
                                       PullRequestService pullRequestService, SecurityService securityService,
                                       TransactionTemplate transactionTemplate,
                                       PermissionValidationService validationService) {
        this.eventPublisher = eventPublisher;
        this.i18nService = i18nService;
        this.pullRequestService = pullRequestService;
        this.securityService = securityService;
        this.transactionTemplate = transactionTemplate;
        this.validationService = validationService;

        pluginSettings = pluginSettingsFactory.createSettingsForKey(PLUGIN_KEY);
    }

    @Nonnull
    @Override
    public AutoUnapproveSettings getSettings(@Nonnull Scope scope) {
        return requireNonNull(scope, "scope")
                .accept(new ScopeVisitor<AutoUnapproveSettings>() {

                    @Override
                    public AutoUnapproveSettings visit(@Nonnull GlobalScope globalScope) {
                        return SimpleAutoUnapproveSettings.builder(globalScope)
                                .enabled(isEnabledForKey(GLOBAL_SETTINGS_KEY).orElse(false))
                                .build();
                    }

                    @Override
                    public AutoUnapproveSettings visit(@Nonnull ProjectScope projectScope) {
                        return isEnabledForKey(getProjectSettingsKey(projectScope))
                                .map(enabled -> SimpleAutoUnapproveSettings.builder(projectScope)
                                        .enabled(enabled)
                                        .build())
                                .orElseGet(() -> visit(Scopes.global()));
                    }

                    @Override
                    public AutoUnapproveSettings visit(@Nonnull RepositoryScope repositoryScope) {
                        return isEnabledForKey(getRepositorySettingsKey(repositoryScope))
                                .map(enabled -> SimpleAutoUnapproveSettings.builder(repositoryScope)
                                        .enabled(enabled)
                                        .build())
                                .orElseGet(() -> visit(Scopes.project(repositoryScope.getProject())));
                    }
                });
    }

    @Nonnull
    @Override
    public AutoUnapproveSettings getSettings(@Nonnull PullRequest pullRequest) {
        return getSettings(Scopes.repository(requireNonNull(pullRequest, "pullRequest").getToRef().getRepository()));
    }

    @Deprecated
    @Override
    public boolean isEnabled(@Nonnull PullRequest pullRequest) {
        return getSettings(pullRequest).isEnabled();
    }

    @Deprecated
    @Override
    public boolean isEnabled(@Nonnull Repository repository) {
        return getSettings(Scopes.repository(requireNonNull(repository, "repository"))).isEnabled();
    }

    @Override
    public boolean removeSettings(@Nonnull Scope scope) {
        validatePermission(requireNonNull(scope, "scope"));
        return scope.accept(new ScopeVisitor<Boolean>() {
            @Override
            public Boolean visit(@Nonnull GlobalScope scope) {
                throw new ArgumentValidationException(i18nService.createKeyedMessage("bitbucket.autounapprove.scope.global.delete"));
            }

            @Override
            public Boolean visit(@Nonnull ProjectScope scope) {

                return pluginSettings.remove(getProjectSettingsKey(scope)) != null;
            }

            @Override
            public Boolean visit(@Nonnull RepositoryScope scope) {
                return pluginSettings.remove(getRepositorySettingsKey(scope)) != null;
            }
        });
    }

    @Deprecated
    @Override
    public void setEnabled(@Nonnull Repository repository, boolean enabled) {
        setSettings(SimpleAutoUnapproveSettings.builder(Scopes.repository(requireNonNull(repository, "repository")))
                .enabled(enabled)
                .build());
    }

    @Override
    public void setSettings(@Nonnull AutoUnapproveSettings settings) {
        validatePermission(requireNonNull(settings, "settings").getScope());

        String key = settings.getScope().accept(new ScopeVisitor<String>() {

            @Override
            public String visit(@Nonnull GlobalScope globalScope) {
                return GLOBAL_SETTINGS_KEY;
            }

            @Override
            public String visit(@Nonnull ProjectScope projectScope) {
                return getProjectSettingsKey(projectScope);
            }

            @Override
            public String visit(@Nonnull RepositoryScope repositoryScope) {
                return getRepositorySettingsKey(repositoryScope);
            }
        });
        if (putSetting(key, settings.isEnabled() ? "1" : "0")) {
            publishEvent(settings.getScope(), settings.isEnabled());
        }
    }

    @Override
    public void withdrawApprovals(@Nonnull PullRequest pullRequest) {
        try {
            transactionTemplate.execute(() -> {
                for (PullRequestParticipant reviewer : pullRequest.getReviewers()) {
                    // the setReviewerStatus method updates the status for the currently authenticated user,
                    // so use SecurityService to impersonate the reviewer we are withdrawing approval for
                    securityService.impersonating(reviewer.getUser(), "Unapproving pull-request on behalf of user")
                            .call(() -> pullRequestService.setReviewerStatus(
                                    pullRequest.getToRef().getRepository().getId(),
                                    pullRequest.getId(),
                                    PullRequestParticipantStatus.UNAPPROVED)
                            );
                }
                return null;
            });
        } catch (IllegalPullRequestStateException e) {
            log.info("[{}] is {}, which prevents withdrawing approvals", pullRequest, pullRequest.getState());
        } catch (DataStoreException e) {
            // We are calling this method through both AutoUnapproveListener and PullRequestVersionMergeCheck and this
            // could happen concurrently due to the asynchronous nature of the events. It doesn't really matter which
            // one wins since either way the approvals will be reset.
            log.debug("Failed to set the reviewer status for pull request {}, which prevents withdrawing approvals. " +
                      "Ignoring as this could be caused by concurrent updates.", pullRequest);
        }
    }

    private static String getProjectSettingsKey(@Nonnull ProjectScope projectScope) {
        return "project." + projectScope.getProject().getId() + ".auto.unapprove";
    }

    private static String getRepositorySettingsKey(@Nonnull RepositoryScope repositoryScope) {
        return "repo." + repositoryScope.getRepository().getId() + ".auto.unapprove";
    }

    private Optional<Boolean> isEnabledForKey(@Nonnull String key) {
        return ofNullable(pluginSettings.get(key)).map("1"::equals);
    }

    private void publishEvent(@Nonnull Scope scope, boolean enabled) {
        scope.accept(new ScopeVisitor<Void>() {

            @Override
            public Void visit(@Nonnull GlobalScope globalScope) {
                eventPublisher.publish(new AutoUnapproveGlobalSettingsChangedEvent(DefaultAutoUnapproveService.this, enabled));
                return null;
            }

            @Override
            public Void visit(@Nonnull ProjectScope projectScope) {
                eventPublisher.publish(new AutoUnapproveProjectSettingsChangedEvent(DefaultAutoUnapproveService.this, projectScope.getProject(), enabled));
                return null;
            }

            @Override
            public Void visit(@Nonnull RepositoryScope repositoryScope) {
                eventPublisher.publish(new AutoUnapproveSettingsChangedEvent(DefaultAutoUnapproveService.this, repositoryScope.getRepository(), enabled));
                return null;
            }
        });
    }

    private boolean putSetting(String key, String value) {
        return !value.equals(pluginSettings.put(key, value));
    }

    private void validatePermission(@Nonnull Scope scope) {
        scope.accept(new ScopeVisitor<Void>() {

            @Override
            public Void visit(@Nonnull GlobalScope globalScope) {
                validationService.validateForGlobal(Permission.ADMIN);
                return null;
            }

            @Override
            public Void visit(@Nonnull ProjectScope projectScope) {
                validationService.validateForProject(projectScope.getProject(), Permission.PROJECT_ADMIN);
                return null;
            }

            @Override
            public Void visit(@Nonnull RepositoryScope repositoryScope) {
                validationService.validateForRepository(repositoryScope.getRepository(), Permission.REPO_ADMIN);
                return null;
            }
        });
    }
}
